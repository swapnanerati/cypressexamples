## Use

1. build the containers with `npm run build`
2. start the web application and run Cypress tests with `npm run up`

## More info

- [Cypress Docker docs](https://on.cypress.io/docker)
- [Cypress continuous integration docs](https://on.cypress.io/ci)
- [docker-compose networking](https://docs.docker.com/compose/networking/)
- Read the excellent [End-to-End Testing Web Apps: The Painless Way](https://mtlynch.io/painless-web-app-testing/)
